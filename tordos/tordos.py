#!/usr/bin/env python
# -*- coding:utf-8 -*-



from tornado.web import Application

import settings
import tordos.core.utils as utils


class TordosApplication(Application):

    def __init__(self):

        settings = utils.setting_from_object(settings)
        self.settings.update(settings)

        handlers = [

        ]



