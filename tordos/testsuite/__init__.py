#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 人生苦短,我用python 

__author__ = 'daqing15@gmail.com'


import os
import re
import sys
import unittest
from traceback import format_exception


__HERE = os.path.dirname(os.path.abspath(__file__))


class TordosDirTemplateCase(unittest.TestCase):

    def tearDown(self):
        self.teardown()

    def setUp(cls):
        self.setup()

    def setup(self):
        pass

    def teardown(self):
        pass

    def assert_equal(self, a, b):
        self.assertEqual(a, b)

    def assert_raises(self,  *args, **kwargs):
        self.assertRaises(*args, **kwargs)

    def assert_traceback_matches(self, callback, expected_tb):
        try:
            callback()
        except Exception, e:
            tb = format_exception(*sys.exc_info())
            if re.search(expected_tb.strip(), ''.join(tb)) is None:
                raise self.fail('Traceback did not match:\n\n%s\nexpected:\n%s'
                                % (''.join(tb), expected_tb))
        else:
            self.fail('Expected Exception here')


def suite():

    from tordos.testsuite import cores

    suite = unittest.TestSuite()
    suite.addTest(cores.suite())