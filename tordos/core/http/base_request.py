#!/usr/bin/env python
# -*- coding:utf-8 -*-


import tornado.web


class BaseRequest(tornado.web.RequestHandler):

    def _prepare_context(self):
        pass


class _Context(dict):
    """ 模板上下文容器 template context container
    如果上下文中的key值不存在，会默认返回空字符串，而不是抛出AttributeError错误
    It will raise TemplateContextError if debug is True and the key
    does not exist.
    The context item also can be accessed through get attribute.
    """
    def __setattr__(self, key, value):
        self[key] = value

    def __hasattr__(self, key):
        if key in self:
            return True
        return False

    def __getattr__(self, key):
        if key in self:
            return self[key]
        else:
            return ""

    def __iter__(self):
        return iter(self.items())

    def __str__(self):
        return str(self)

class ErrorHandler(BaseRequest):
    """raise 404 error if url is not found.
    fixed tornado.web.RequestHandler HTTPError bug.
    """
    def prepare(self):
        self._prepare_context()
        self.set_status(404)
        raise tornado.web.HTTPError(404)