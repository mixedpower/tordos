#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 人生苦短,我用python 

__author__ = 'daqing15@gmail.com'


def setting_from_object(obj):
    settings = dict()
    for key in dir(obj):
        if key.isupper():
            settings[key.lower()] = getattr(obj, key)
    return settings