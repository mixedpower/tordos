#!/usr/bin/env python
# -*- coding:utf-8 -*-


import tornado.httpserver
import tornado.ioloop

from tordos.tordos import TordosApplication


def run():
    app = TordosApplication()
    http_server = tornado.httpserver.HTTPServer(app, xheaders=True)
    http_server.listen(port=8080)
    tornado.ioloop.IOLoop.instance().start()
    print '------------------ everything is running ok ---------------'


if __name__ == "__main__":
    run()