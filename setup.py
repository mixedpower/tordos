#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 人生苦短,我用python 

__author__ = 'daqing15@gmail.com'

import sys

from setuptools import setup, Extension, Feature

setup(
    name='Tordos',
    version='0.1',
    url='http://fashtime.com/',
    license='MIT',
    author=__author__,
    author_email=__author__,
    description='A tornado web develop dir template',
    long_description=__doc__,
    zip_safe=False,
    classifiers=[
        'Development Status :: 5 - Dev',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    packages=['tordos', 'tordos.core', 'tordos.modules',
              'tordos.modules.core'],
    extras_require={'i18n': ['Babel>=0.8'], 'tornado':['torando>=2.4']},
    test_suite='tordos.testsuite.suite',
    include_package_data=True,
    entry_points="""
    [babel.extractors]
    jinja2 = jinja2.ext:babel_extract[i18n]
    """,
    features={'debugsupport': debugsupport},
    **extra
)