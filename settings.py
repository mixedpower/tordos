#!/usr/bin/env python
# -*- coding:utf-8 -*-


__author__ = 'daqing15@gmail.com'

import os

_ROOT = os.path.dirname(__file__)

DEBUG = True
COOKIE_SECRET = 'Y2o1TzK2YQAGEYdkL5gmueJIFuY37EQm92XsTo1v/Wi='
LOGIN_URL = '/login'
XSRF_COOKIE = True
GZIP = True
DEFAULT_LOCALE = 'zh_CN' #'zh_CN'
PERMANENT_SESSION_LIFETIME = 1 # days If set number the session lives for value days.


I18N_SUPPORT = 'csv'            #国际化参数，csv or babel
SESSION_TYPE = 'dict'           #memcached, redis, mongodb, dict 形式的session模式
ORM_TYPE     = 'sa'             # sa(sqlalchemy), peewee, mongo
TMPL_TYPE    = 'default'        # default(tornado), jinja2, mako
