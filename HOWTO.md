

如何使用该torando web 目录结构
以下的描述是本人希望该项目能做到的愿景，希望各大侠们多提宝贵意见

1.先下载该项目模板

2.查看项目里的各个模块信息
开始：首先该模板只是假设，每个人都可以修改其结构，如果觉得该项目结构不合理或有更好的方式，
    请联系本人：fashtime.com@gmail.com 修改或讨论

2.1 目录结构
    tordos                          -- 顶层目录
      |-i18n                        -- 国际化文件存放处，配置参数【I18N_SUPPORT】支持csv或babel
      |-testsuite                   -- 单元测试（尝试测试每个模块的可用性）
      |-static
      |-templates
      |-tordos                      -- web项目python模块的顶层包
        |-core                      -- 放了一些收集到tornado做web项目需要用到的外部支持模块
          |-http
            |-base_request.py       -- 每个RequestHandler的父类，有自己的特定修改，可在此处做修改
            |-form.py               -- 支持wtform
            |-routing.py            -- 支持flask的路由映射方式：example: @route('/','ddd') def index():pass
            |-session.py            -- session的支持：memcached, redis, mongodb, dict等方式(配置参数SESSION_TYPE)
          |-orm
            |-sqlalchemy.py            -- 集成SQLAlchemy orm框架(配置参数：ORM_TYPE)
            |-peewee.py                -- 集成peeweee orm框架
            |-mongo.py                 -- 集成mongodb数据库的支持
          |-tmpl
            |-jinja.py                 -- 集成jinja2模板(配置参数：TMPL_TYPE)
            |-mako.py                  -- 集成mako模板
          |-exts
            |-cache.py                 -- 缓存支持
            |-permission.py            -- 权限支持
          |-queue
            |-...
          models.py                    -- 模型类
          utils.py                     -- 常用工具方法放置处
        |-modules                      -- 每个子系统的存放区，例如博客等等
          |-blog                       -- 博客例子
            |-controller.py            -- 每个请求的处理handler
            |-model.py                 -- 该模块用到的自定义数据类
            |-uimodules.py             -- 若使用了tornado模板，可以定义该文件
            |-static
            |-template


3.快速开始
3.1 在modules下新建一个python模块，参考上面举出的例子（也可拷贝一份）
3.2 在建好的模块的controller.py中定义自己提供给客户端的post/get URI
3.3 跑起在某个端口
